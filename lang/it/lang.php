<?php return [
    'plugin' => [
        'name' => 'Prodotti',
        'description' => 'Gestione Prodotti Quivi',
    ],
    'menu' => [
        'main' => 'Prodotti',
        'side' => [
            'products' => 'Prodotti',
            'bundles' => 'Pacchetti',
            'types' => 'Tipologie Prodotti',
            'features' => 'Caratteristiche Prodotti',
            'vendors' => 'Fornitori',
            'versions' => 'Versioni Prodotti'
        ]
    ],
    'feature' => [
        'feature' => 'Caratteristica',
        'features' => 'Caratteristiche',
        'name' => 'Nome Caratteristica',
        'notes' => 'Note',
        'list' => 'Lista Caratteristiche'
    ],
    'option' => [
        'option' => 'Opzione',
        'options' => 'Opzioni',
        'name' => 'Nome Opzione',
        'parent_option' => 'Dipendente da Opzione',
        'parent_option_empty' => '--',
        'child_options' => 'SubOpzioni',
        'notes' => 'Note',
        'name' => 'Nome Opzione',
        'descr' => 'Descrizione Opzione',
        'list' => 'Lista Opzioni',
        'price' => 'Prezzo Opzione',
        'cost' => 'Costo Opzione',
        'vendor' => 'Fornitore Opzione',
        'parent_vendor_empty' => '--',
        'factor' => 'Moltiplicatore',
        'is_enabled' => 'Opzione Abilitata',
        'is_upsell' => 'Opzione Upsell',
        'is_customizable' => 'Opzione Personalizzabile',
        'tabs' => [
            'details' => 'Dettagli Opzione',
            'vendor' => 'Fornitore Opzione'
        ]
    ],
    'product' => [
        'product' => 'Prodotto',
        'products' => 'Prodotti',
        'create' => 'Crea nuovo Prodotto',
        'update' => 'Modifica Prodotto',
        'preview' => 'Anteprima Prodotto',
        'name' => 'Nome Prodotto',
        'slug' => 'Slug',
        'code' => 'Codice Prodotto',
        'ean_code' => 'EAN Code',
        'type' => 'Tipologia Prodotto',
        'descr' => 'Descrizione Prodotto',
        'technical_descr' => 'Descrizione tecnica',
        'technical_descr_comment' => 'Eventuale impiego di soluzioni innovative...',
        'price' => 'Prezzo',
        'vendors' => 'Fornitori',
        'vendors_comment' => 'Gestisci i fornitori/provider del prodotto/servizio',
        'versions' => 'Versioni',
        'versions_comment' => 'Gestisci le versioni del prodotto/servizio',
        'owner' => 'Responsabile Prodotto',
        'owner_empty' => '-- Scegli il responsabile --',
        'brand' => 'Brand / Marca',
        'brand_empty' => '-- Scegli brand --',
        'photo' => 'Immagine principale',
        'featured_photo_horizontal' => 'Immagine principale (formato orizzontale)',
        'featured_photo_horizontal_comment' => 'Immagine di copertina del prodotto in formato orizzontale',
        'featured_photo_vertical' => 'Immagine principale (formato verticale)',
        'featured_photo_vertical_comment' => 'Immagine di copertina del prodotto in formato verticale',
        'media' => 'Media',
        'gallery' => 'Galleria',
        'list' => 'Lista Prodotti',
        'techniques_pivot_model' => 'Lavorazioni e Attività per Delivery Prodotto',
        'vendor' => [
            'name' => 'Nome Prodotto Fornitore',
            'code' => 'Codice Prodotto Fornitore',
            'cost' => 'Costo Prodotto',
            'notes' => 'Note'
        ],
        'technique' => [
            'parent_product_technique' => 'Dipendente da Attività / Lavorazione',
            'option' => 'Dipendente da Opzione',
            'empty_options' => 'Nessuna Opzione specifica',
            'parent_technique' => 'Dipendente da altra Lavorazione',
            'empty_parent_techniques' => 'Nessuna altra Lavorazione propedeutica',
            'notes' => 'Note sulla lavorazione',
            'step' => 'Step di Delivery',
            'default' => 0
        ],
        'tabs' => [
            'details' => 'Dettagli Prodotto',
            'options' => 'Opzioni',
            'vendors' => 'Fornitori',
            'delivery' => 'Delivery',
            'versions' => 'Versioni'
        ],
        'version' => [
            'version' => 'Versione',
            'versions' => 'Versioni',
            'name' => 'Nome/Codice Versione',
            'owner' => 'Responsabile Prodotto',
            'owner_empty' => '-- Scegli il responsabile --',
            'attachments' => 'Allegati',
            'alert' => '<strong>Attenzione.</strong> Sarà considerata revisione corrente l\'ultima inserita in termini di tempo.',
            'tabs' => [
                'details' => 'Dettaglio Versione'
            ]
        ],
    ],
    'bundle' => [
        'bundle' => 'Pacchetto',
        'bundles' => 'Pacchetti',
        'name' => 'Nome Pacchetto',
        'descr' => 'Descrizione',
        'photo' => 'Immagine di copertina',
        'gallery' => 'Galleria',
        'product' => [
            'products' => 'Prodotti',
            'comment' => 'Prodotti nel Pacchetto',
            'name' => 'Nome del Prodotto nel Pacchetto',
            'quantity' => 'Quantità',
            'price' => 'Prezzo'
        ],
        'tabs' => [
            'details' => 'Dettagli Pacchetto',
            'media' => 'Media',
            'products' => 'Prodotti'
        ]
    ],
    'vendor' => [ 
        'vendor' => 'Fornitore',
        'vendors' => 'Fornitori',
        'name' => 'Nome Fornitore',
        'new' => 'Nuovo Fornitore',
        'edit' => 'Modifica Fornitore',
        'list' => 'Lista Fornitori'
    ],
    'version' => [
        'version' => 'Versione',
        'versions' => 'Versioni',
        'name' => 'Nome/Codice Versione',
        'owner' => 'Responsabile Prodotto',
        'owner_empty' => '-- Scegli il responsabile --',
        'attachments' => 'Allegati',
        'alert' => '<strong>Attenzione.</strong> Sarà considerata revisione corrente l\'ultima inserita in termini di tempo.',
        'tabs' => [
            'details' => 'Dettaglio Versione'
        ]
    ]
];