<?php return [
    'plugin' => [
        'name' => 'Product',
        'description' => 'Quivi Products Management',
    ],
    'quivi' => [
        'product::lang' => [
            'menu' => [
                'products' => 'Products',
            ],
            'quivi' => [
                'product::lang' => [
                    'menu' => [
                        'types' => 'Types',
                    ],
                ],
                'product::slug' => 'Slug',
                'product::type' => 'Type',
                'product::price' => 'Price',
                'product::price_a' => 'Price A',
                'product::price_b' => 'Price B',
                'product::price_c' => 'Price C',
                'product::cost' => 'Cost',
            ],
        ],
    ],
];