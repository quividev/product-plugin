<?php namespace Quivi\Product\Models;

use October\Rain\Database\Pivot;

/**
 * User-Role Pivot Model
 */
class ProductVendorPivot extends Pivot
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Rules
     */
    public $rules = [
//        'clearance_level' => 'required|min:3',
    ];

    public $moneyFields = [
        'product_cost' => [
            'amountColumn' => 'cost',
            'currencyIdColumn' => 'currency_id'
        ]
    ];

    public $implement = [
        'Initbiz.Money.Behaviors.MoneyFields'
    ];

}