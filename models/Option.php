<?php namespace quivi\Product\Models;

use Model;

/**
 * Model
 */
class Option extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_options';

    public $attachMany = [
        'images' => 'System\Models\File',
        'attachments' => 'System\Models\File',
    ];

    public $belongsTo = [
        'feature' => ['Quivi\Product\Models\Feature'],
        'parent_option' => [
            'Quivi\Product\Models\Option',
            'key' => 'option_id',
            'otherKey' => 'id'
        ],
        'child_options' => [
            'table' => 'quivi_product_options',
            'Quivi\Product\Models\Option',
            'key' => 'id',
            'otherKey' => 'option_id'
        ],
    ];

    public $hasMany = [
        'techniques' => 'Quivi\Profile\Models\Technique',
    ];
    public $belongsToMany = [
        'products' => 'Quivi\Product\Models\Product',
        'techniques' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_product_techniques',
            'timestamps' => true,
            'order' => 'name'
        ],
        'techniques_count' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_product_techniques',
            'count' => true
        ],
    ];

    public function getFeatureAndOptionAttribute()
    {
        return $this->feature->name.": ".$this->name;
    }



}
