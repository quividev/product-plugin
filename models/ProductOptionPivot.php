<?php namespace Quivi\Product\Models;

use October\Rain\Database\Pivot;

/**
 * User-Role Pivot Model
 */
class ProductOptionPivot extends Pivot
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Rules
     */
    public $rules = [
//        'clearance_level' => 'required|min:3',
    ];

    public $belongsTo = [
        'vendor' => 'Quivi\Product\Models\Vendor',
        'key' => 'vendor_id',
        'otherKey' => 'id'
    ];

    public $moneyFields = [
        'option_price' => [
            'amountColumn' => 'price',
            'currencyIdColumn' => 'currency_id'
        ],
        'option_cost' => [
            'amountColumn' => 'cost',
            'currencyIdColumn' => 'currency_id'
        ]
    ];

    public $implement = [
        'Initbiz.Money.Behaviors.MoneyFields'
    ];

}