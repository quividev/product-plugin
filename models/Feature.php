<?php namespace quivi\Product\Models;

use Model;

/**
 * Model
 */
class Feature extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        "type"  =>  "required",
        "name"  =>  "required",
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_features';

    public $belongsTo = [
        'type' => ['Quivi\Product\Models\Type'],
    ];
    

    public $hasMany = [
        'products' => 'Quivi\Product\Models\Product',
	    'options' => 'Quivi\Product\Models\Option'
    ];
}
