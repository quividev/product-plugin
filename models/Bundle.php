<?php namespace Quivi\Product\Models;

use Model;

/**
 * Model
 */
class Bundle extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'name',
        ['slug', 'index' => true]
    ];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_bundles';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $with = ['products'];


    public $belongsToMany = [
        'products' => [
            'Quivi\Product\Models\Product',
            'pivot' => ['name', 'descr', 'price', 'currency_id', 'quantity'],
            'table' => 'quivi_product_bundles_products',
            'timestamps' => true,
            'pivotModel' => 'Quivi\Product\Models\BundleProductPivot',
        ],
    ];

    public $attachMany = [
        'bundle_gallery' => 'System\Models\File'
    ];
/*

    public function getAccomodationByType($type_slug = null) {

        return $this->products->filter(function($product) use ($type_slug) {

            return $product->type->slug == $type_slug;

        });
    }

    // Prezzo di tutti i prodotti non opzionali
    public function getBasePriceAttribute() {

        return $this->products->reduce(function ($subtotal, $product) {
            return ($product->pivot->is_optional == 0) ? $subtotal + $product->price : $subtotal;
        });

    }

*/

}
