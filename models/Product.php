<?php namespace Quivi\Product\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];

    protected $with = ['type','owner','brand'];

    protected $appends = ['formatted_price','formatted_cost','margin','formatted_margin'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_products';

    public $belongsTo = [
        'type' => 'Quivi\Product\Models\Type',
        'brand' => 'Quivi\Product\Models\Brand',
        'owner' => [
            'Quivi\Profile\Models\User', 
            'key'                           => 'owner_id', 
            'otherKey'                      => 'id',
            'scope'                         => 'productOwner'
        ],
    ];

    public $hasMany = [
        'versions' => 'Quivi\Product\Models\Version',
    ];
 
    public $belongsToMany = [
        'vendors' => [
            'Quivi\Product\Models\Vendor',
            'pivot' => ['name', 'code', 'descr', 'cost'],
            'table' => 'quivi_product_products_vendors',
            'timestamps' => true,
            'pivotModel' => 'Quivi\Product\Models\ProductVendorPivot',
        ],
        'options' => [
            'Quivi\Product\Models\Option',
            'pivot' => [
                'name', 
                'descr', 
                'vendor_id', 
                'price', 
                'cost', 
                'currency_id', 
                'is_enabled', 
                'is_upsell', 
                'is_customizable',
                'factor'
            ],
            'table' => 'quivi_product_products_options',
            'pivotModel' => 'Quivi\Product\Models\ProductOptionPivot',
        ],
        'techniques' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_product_products_techniques',
            'timestamps' => true,
            'order' => 'name'
        ],
        'techniques_count' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_product_products_techniques',
            'count' => true
        ],
        'techniques_pivot_model' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_product_products_techniques',
            'pivot' => ['parent_technique_id','option_id','step','notes'],
            'timestamps' => true,
            'pivotModel' => 'Quivi\Product\Models\ProductTechniquePivot',
        ],
        'users' => [
            'Quivi\Profile\Models\User',
            'table' => 'quivi_product_products_users',
        ]
    ];

    public $attachMany = [
        'product_gallery' => 'System\Models\File'
    ];

    public $moneyFields = [
        'product_price' => [
            'amountColumn' => 'price',
            'currencyIdColumn' => 'currency_id'
        ]
    ];

    public $implement = [
        'Initbiz.Money.Behaviors.MoneyFields'
    ];


    protected $legacy = 
    [
        'product_type' => 'type_id',
        'product_name' => 'name',
        'product_descr' => 'descr',
        'product_slug' => 'slug',
        'product_class' => 'class',
        'product_price' => 'price',
        'product_price_reseller_a' =>  'price_reseller_a',
        'product_price_reseller_b' => 'price_reseller_b',
        'product_price_reseller_c' => 'price_reseller_c',
        'product_cost' => 'cost',
        'product_photo' => 'photo',
        'product_gallery' => 'product_gallery',
        'product_created_at' => 'created_at',
        'product_updated_at' => 'updated_at',
        'product_enabled' => 'is_enabled'
    ];


    public function ingest($object){

        foreach ($this->legacy as $old => $new){
            if ($new && isset($object->$old)) {
                $this->$new = $object->$old;
            }
        }
    }

    public function getFormattedPriceAttribute() {

        return number_format($this->price/100,2,",","")." EUR";

    }

    public function getFormattedCostAttribute() {

        return number_format($this->cost/100,2,",","")." EUR";

    }

    public function getMarginAttribute() {

        return $this->price - $this->cost;

    }

    public function getFormattedMarginAttribute() {

        return number_format($this->getMarginAttribute()/100,2,",","")." EUR";

    }



}
