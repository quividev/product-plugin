<?php namespace Quivi\Product\Models;

use October\Rain\Database\Pivot;

/**
 * Product-Technique Pivot Model
 */
class ProductTechniquePivot extends Pivot
{
    public $belongsTo = [
        'parent_technique' => [
            'Quivi\Profile\Models\Technique',
            'key' => 'parent_technique_id',
            'otherKey' => 'id'
        ],
        'option' => [
            'Quivi\Product\Models\Option',
            'key' => 'option_id',
            'otherKey' => 'id'
        ]
    ];


}