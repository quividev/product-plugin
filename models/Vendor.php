<?php namespace Quivi\Product\Models;

use Model;
use Quivi\Profile\Models\Profile as ProfileModel;

/**
 * Model
 */
class Vendor extends ProfileModel
{
    public function scopeVendors($query) {
        $query->with('groups')->whereHas('groups', function($q){
            $q->where('code','vendor');
        });
    }

}
