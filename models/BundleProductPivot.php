<?php namespace Quivi\Product\Models;

use October\Rain\Database\Pivot;

/**
 * Model
 */
class BundleProductPivot extends Pivot
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_bundles_products';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
    ];

    public $moneyFields = [
        'product_price' => [
            'amountColumn' => 'price',
            'currencyIdColumn' => 'currency_id'
        ]
    ];
    public $implement = [
        'Initbiz.Money.Behaviors.MoneyFields'
    ];
}
