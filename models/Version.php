<?php namespace quivi\Product\Models;

use Model;

/**
 * Model
 */
class Version extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'  =>  'required',
    ];
    
    public $belongsTo = [
        'product' => ['Quivi\Product\Models\Product'],
        'owner' => [
            'Quivi\Profile\Models\User', 
            'key'                           => 'owner_id', 
            'otherKey'                      => 'id',
            'scope'                         => 'productOwner'
        ],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_product_versions';


    public $attachMany = [
        'images' => 'System\Models\File',
        'attachments' => 'System\Models\File',
    ];
}
