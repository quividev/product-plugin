<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductProductsUsers extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_products_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->primary(['product_id', 'user_id'], 'product_user');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_products_users');
    }
}
