<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviProductProducts2 extends Migration
{
    public function up()
    {
        Schema::table('quivi_product_products', function($table)
        {
            $table->string('packaging_size', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('quivi_product_products', function($table)
        {
            $table->dropColumn('packaging_size');
        });
    }
}
