<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviProductProductsOptions extends Migration
{
    public function up()
    {
        Schema::table('quivi_product_products_options', function($table)
        {
            $table->decimal('factor', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('quivi_product_products_options', function($table)
        {
            $table->dropColumn('factor');
        });
    }
}
