<?php namespace quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductOptions extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_options', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('feature_id');
            $table->integer('option_id')->unsigned()->nullable();
            $table->string('name', 255);
            $table->text('descr')->nullable();
            $table->string('image')->nullable();
            $table->text('data')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_options');
    }
}
