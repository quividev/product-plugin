<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductProductsVendors extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_products_vendors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->string('name', 255)->nullable();
            $table->string('code', 255)->nullable();
            $table->string('descr', 4096)->nullable();
            $table->string('notes', 4096)->nullable();
            $table->integer('cost')->default(0);
            $table->integer('currency_id')->nullable();
            $table->timestamps();
            $table->primary(['product_id', 'vendor_id'], 'product_vendor');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_products_vendors');
    }
}
