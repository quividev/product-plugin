<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductProductsTechniques extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_products_techniques', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('technique_id')->unsigned();
            $table->integer('option_id')->unsigned()->nullable();
            $table->integer('parent_technique_id')->unsigned()->nullable();
            $table->string('notes')->nullable();
            $table->integer('step')->default(0);
            $table->timestamps();
            $table->unique(['product_id', 'technique_id'], 'product_technique');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_products_techniques');
    }
}
