<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductProducts extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('type_id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('code', 255)->nullable();
            $table->string('ean_code', 255)->nullable();
            $table->mediumText('descr')->nullable();
            $table->integer('price')->default(0);
            $table->integer('currency_id')->nullable();
            $table->string('photo', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->boolean('is_enabled')->default(1);
            $table->timestamp('published_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_products');
    }
}
