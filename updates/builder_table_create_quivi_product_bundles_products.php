<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductBundlesProducts extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_bundles_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('bundle_id');
            $table->integer('product_id');
            $table->string('name')->nullable();
            $table->string('descr')->nullable();
            $table->integer('quantity')->default(1);
            $table->integer('price')->default(0);
            $table->integer('currency_id')->nullable();
            $table->timestamps();
            $table->primary(['bundle_id', 'product_id'], 'bundle_product');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_bundles_products');
    }
}
