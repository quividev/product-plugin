<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviProductProductsOptions2 extends Migration
{
    public function up()
    {
        Schema::table('quivi_product_products_options', function($table)
        {
            $table->boolean('is_customizable')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('quivi_product_products_options', function($table)
        {
            $table->dropColumn('is_customizable');
        });
    }
}
