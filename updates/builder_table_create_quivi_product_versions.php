<?php namespace quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductVersions extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_versions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('product_id');
            $table->integer('owner_id')->unsigned()->nullable();
            $table->string('name', 150)->nullable();
            $table->string('hash', 4096)->nullable();
            $table->text('data')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->dateTime('revoked_at')->nullable();
            $table->dateTime('expiration_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_versions');
    }
}
