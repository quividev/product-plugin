<?php namespace Acme\Users\Updates;

use Seeder;
use RainLab\User\Models\UserGroup;
use RainLab\User\Models\User;
use DB;

class SeedUserGroupsTable extends Seeder
{
    public function run()
    {
        DB::table('user_groups')->where("id","=", "3")->delete();
        DB::table('user_groups')->where("id","=", "4")->delete();
        //DB::table('users_groups')->where("user_group_id","=", "3")->delete();
        //DB::table('users_groups')->where("user_group_id","=", "4")->delete();
        
        //User::truncate();

        $user = UserGroup::create([
            'id'                    => '3',
            'name'                  => 'Fornitore',
            'code'                  => 'vendor',
            'description'           => 'Fornitore',
        ]);

        $user = UserGroup::create([
            'id'                    => '4',
            'name'                  => 'Responsabile di Prodotto',
            'code'                  => 'product_owner',
            'description'           => 'Responsabile di Prodotto',
        ]);

    }
}