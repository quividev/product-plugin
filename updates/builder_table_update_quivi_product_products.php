<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviProductProducts extends Migration
{
    public function up()
    {
        Schema::table('quivi_product_products', function($table)
        {
            $table->integer('brand_id')->nullable()->unsigned();
            $table->integer('owner_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('quivi_product_products', function($table)
        {
            $table->dropColumn('brand_id');
            $table->dropColumn('owner_id');
        });
    }
}
