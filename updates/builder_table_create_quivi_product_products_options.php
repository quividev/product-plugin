<?php namespace Quivi\Product\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProductProductsOptions extends Migration
{
    public function up()
    {
        Schema::create('quivi_product_products_options', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('option_id')->unsigned()->nullable();
            $table->string('name', 255)->nullable();
            $table->string('descr', 4096)->nullable();
            $table->integer('price')->default(0);
            $table->integer('cost')->default(0);
            $table->integer('currency_id')->nullable();
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->boolean('is_enabled')->default(false);
            $table->boolean('is_upsell')->default(false);
            $table->timestamps();
            $table->primary(['product_id', 'option_id'], 'product_option');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_product_products_options');
    }
}
