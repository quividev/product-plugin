<?php namespace Quivi\Product;

use System\Classes\PluginBase;

use Rainlab\User\Models\User as UserModel;
use Quivi\Profile\Models\Profile as ProfileModel;

use Event;


class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
    }

    public function boot(){

        UserModel::extend(function($model){
            $model->addDynamicMethod('scopeProductOwner', function($query) use ($model) {
                $query->with('groups')->whereHas('groups', function($q){
                    $q->where('code','product_owner');
                });
            });
            $model->addDynamicMethod('scopeVendor', function($query) use ($model) {
                $query->with('groups')->whereHas('groups', function($q){
                    $q->where('code','supplier');
                });
            });
        });

        ProfileModel::extend(function($model){

            $model->belongsToMany['products'] = [
                'Quivi\Product\Models\Product',
                'pivot' => ['name', 'descr', 'cost'],
                'table' => 'quivi_product_products_vendors',
                'timestamps' => true,
                'pivotModel' => 'Quivi\Product\Models\ProductVendorPivot',
            ];

        });

        Event::listen('backend.menu.extendItems', function($manager) {            

            Event::fire('backend.menu.extendItems.product', [$manager]);
    
        });

    }
}
