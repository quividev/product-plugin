<?php namespace quivi\Product\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Features extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $relationConfig = 'config_relation.yaml';

    protected $optionTechniquePivotFormWidget;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Quivi.Product', 'main-menu-item', 'side-menu-item-features');
    }

}
